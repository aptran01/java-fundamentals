package com.training.pets;
public class Pet {

    public static int numPets = 0;

    public int numLegs;
    private String name;


    public Pet(){
        numPets++;
    }

    public void feed(){
        System.out.println("Feed generic pet some generic pet food");
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
