package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class ooFundamentalsMain {

    public static void main(String[] args) {

        Pet myPet = new Pet();
        myPet.setNumLegs(4);
        myPet.setName("Mycroft");

        System.out.println(myPet.getNumLegs());
        System.out.println(myPet.getName());
        myPet.feed();

//        ----------------------------

        Dragon myDragon = new Dragon();
        myDragon.setNumLegs(4);
        myDragon.setName("Mycroft");

        System.out.println(myDragon.getNumLegs());
        System.out.println(myDragon.getName());
        myDragon.feed("goats");
        myDragon.breatheFire();

//        ----------------------------

        Dragon secondDragon = new Dragon();
        secondDragon.setNumLegs(4);
        secondDragon.setName("Mycroft");

        System.out.println(secondDragon.getNumLegs());
        System.out.println(secondDragon.getName());
        secondDragon.feed();
        secondDragon.breatheFire();


    }
}
