package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

import java.util.ArrayList;

public class ArraysMain {

    public static void main(String[] args) {
        ArrayList<Pet> myPetList = new ArrayList<>();

        int[] intArray = {5, 32, 64, 34, 77};

        Pet[] petArray = new Pet[10];


        Pet firstPet = new Pet();
        firstPet.setName("First Pet");
        petArray[0] = firstPet;

        Dragon myDragon = new Dragon("spyro");
        petArray[1] = myDragon;

        for (int i = 0; i <= petArray.length; i++) {
            System.out.println(petArray[i]);
        }

        petArray[0].feed();
//        petArray[1].feed("goats");

    }
}
